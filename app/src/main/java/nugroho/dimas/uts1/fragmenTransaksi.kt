package nugroho.dimas.uts1



import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.transaksi.*
import kotlinx.android.synthetic.main.transaksi.view.*

class fragmenTransaksi : Fragment(), View.OnClickListener {
    lateinit var  fr  :  FragmentTransaction
    lateinit var thisparent : fragmenInti
    lateinit var fragEdit: fragEdit
    lateinit var v : View
    lateinit var db : SQLiteDatabase
    lateinit var build : AlertDialog.Builder
    lateinit var  adaplist : SimpleCursorAdapter
    var id_jen : String =""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        thisparent = activity as fragmenInti
        fragEdit = fragEdit()
        build = AlertDialog.Builder(thisparent)
        db = thisparent.getdatUang()
        v = inflater.inflate(R.layout.transaksi,container,false)
        v.tam.setOnClickListener(this)
        v.lsprod.setOnItemClickListener(clik)
        v.button.setOnClickListener(this)
        return v


    }

    fun delete(id_jen : String){
        db.delete("transaksi","keterangan = '$id_jen'",null)

        showtrans()

    }

    val indel = DialogInterface.OnClickListener { dialog, which ->
        delete(id_jen)

    }

    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_jen = c.getString(c.getColumnIndex("keterangan"))
        //v.inputnil.setText(c.getString(c.getColumnIndex("nama_jenis")))


    }

    override fun onStart() {
        super.onStart()
        showtrans()
    }
    fun showtrans(){

        var  sql = "select a.nama_jenis as _id, b.nama_kategori , c.tgl, c.uang , c.keterangan from transaksi as c,jenis a,kategori b where c.id_jenis = a.id_jenis and c.id_kategori = b.id_kategori"
        val c: Cursor = db.rawQuery(sql, null)
        adaplist = SimpleCursorAdapter(
            thisparent,
            R.layout.isi,
            c,
            arrayOf("_id", "nama_kategori", "tgl","uang","keterangan"),
            intArrayOf(R.id.jenis, R.id.kategori, R.id.tgl3,R.id.uang,R.id.keterangan),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsprod.adapter = adaplist
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tam->{
                fr = activity!!.supportFragmentManager.beginTransaction()
                fr.replace(R.id.FrameLayout,fragEdit).commit()
                fr.addToBackStack(null)

            }
            R.id.button->{
                build.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah anda Yakin Menghapus Data ini !!!")
                    .setPositiveButton("Ya", indel)
                    .setNegativeButton("Tidak", null)
                build.show()
            }
        }

    }

//


}
