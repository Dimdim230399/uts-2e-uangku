package nugroho.dimas.uts1



import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.edite_actifity.*
import kotlinx.android.synthetic.main.edite_actifity.view.*
import java.util.*


class fragEdit : Fragment(),View.OnClickListener,AdapterView.OnItemSelectedListener {


    var bula = 0
    var tahun = 0
    var hari = 0

    lateinit var thisparent: fragmenInti
    lateinit var db: SQLiteDatabase
    lateinit var v: View
    lateinit var LVjen: SimpleCursorAdapter
    lateinit var adapspkat: SimpleCursorAdapter
    lateinit var fr: FragmentTransaction
    lateinit var fragmenTransaksi: fragmenTransaksi
    var nama_jen: String = ""
    var nmkat: String = ""
    var cal: Calendar = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.edite_actifity, container, false)
        thisparent = activity as fragmenInti
        db = thisparent.getdatUang()
        fragmenTransaksi = fragmenTransaksi()
        v.aa.setOnClickListener(this)
        v.settanggal.setOnClickListener(this)
        v.spkat.onItemSelectedListener = this
        v.listKat.setOnItemClickListener(clik)
        return v
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spkat.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val b: Cursor = adapspkat.getItem(position) as Cursor
        nmkat = b.getString(b.getColumnIndex("_id"))
        Toast.makeText(activity!!.baseContext,nmkat,Toast.LENGTH_SHORT).show()
        showjenis(position.toString())
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.aa->{

                var sqljen = "select id_jenis from jenis where nama_jenis = '$nama_jen'"
                var sqlkat = "select id_kategori from kategori where nama_kategori = '$nmkat'"

                val b: Cursor = db.rawQuery(sqlkat, null)
                val a: Cursor = db.rawQuery(sqljen, null)
                if (b.count > 0 &&  a.count > 0){
                    b.moveToFirst()
                    a.moveToFirst()
                    instrans(
                        a.getInt(a.getColumnIndex("id_jenis")),
                        b.getInt(b.getColumnIndex("id_kategori")),
                        "$hari - $bula -$tahun",
                        harga.text.toString(),
                        keterangan.text.toString()
                    )

                }
                fr = activity!!.supportFragmentManager.beginTransaction()
                fr.replace(R.id.FrameLayout,fragmenTransaksi).commit()
                fr.addToBackStack(null)
            }
            }
            R.id.settanggal->{
                val datePickerDialog =
                    DatePickerDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->

                        tanggal.text= "$dayOfMonth" +
                                "-${monthOfYear + 1}-$year"
                        bula = monthOfYear + 1
                        tahun = year
                        hari = dayOfMonth

                    }, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))
                datePickerDialog.onDateSetListener
                datePickerDialog.setTitle("Masukan Tanggal Transaksi")
                datePickerDialog.setAccentColor(resources.getColor(R.color.colorPrimary))
                datePickerDialog.setOkText("Pilih")
                datePickerDialog.setCancelText("Batal")
                datePickerDialog.show(activity!!.fragmentManager, "DatePickerDialog")
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showkat()

        bula = cal.get(Calendar.MONTH) + 1
        tahun = cal.get(Calendar.YEAR)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        v.tanggal.text = "$hari-$bula-$tahun"

    }

    fun showjenis(jen : String) {
        var sql = ""

        if (jen.equals("1")) {
            sql = "select  m.nama_jenis as _id , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori and p.nama_kategori ='pengeluaran'"
        }else{
            sql = "select  m.nama_jenis as _id , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori and p.nama_kategori ='pemasukan'"
        }
        //sql = "select m.id_jenis as _id , m.nama_jenis , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori "

        val c: Cursor = db.rawQuery(sql, null)
        LVjen = SimpleCursorAdapter(
            thisparent,
            R.layout.isi_kat,
            c,
            arrayOf("_id", "nama_kategori"),
            intArrayOf(R.id.nama),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.listKat.adapter = LVjen
    }


    fun showkat() {
        val c: Cursor = db.rawQuery(
            "select nama_kategori as _id from kategori order by nama_kategori asc",
            null
        )
        adapspkat= SimpleCursorAdapter(
            thisparent, android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        adapspkat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spkat.adapter = adapspkat
        v.spkat.setSelection(0)
    }
    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        nama_jen = c.getString(c.getColumnIndex("_id"))
        // v.inputnil.setText(c.getString(c.getColumnIndex("nama_jenis")))
        //nmkat = c.getString(c.getColumnIndex(""))


    }


    // "create table transaksi(id_trans integer primary key autoincrement,id_user text , id_jenis text not null , id_kategori text not null,tgl text not null ,uang int not null,keterangan text not null)"
    fun instrans(id_jenis: Int, id_kategori: Int, tgl: String, uang: String, keterangan: String) {
        val cv: ContentValues = ContentValues()
        cv.put("id_jenis", id_jenis)
        cv.put("id_kategori", id_kategori)
        cv.put("tgl", tgl)
        cv.put("uang", uang)
        cv.put("keterangan", keterangan)
        db.insert("transaksi", null, cv)

    }




}


